Write-Host Richte arabisches und persisches Tastatur-Layout ein... -ForegroundColor Yellow

$LanguageList = Get-WinUserLanguageList

#Adds arabisch-Egypt, Persisch to layouts:
$LanguageList.Add("ar-EG")
$LanguageList.Add("fa-IR")

#sets the entries we added above. 
Set-WinUserLanguageList $LanguageList

Write-Host Installiere chocolatey und weitere Software für das System -ForegroundColor Yellow

#Install Chocolatey
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))

#Install Software with choclatey
choco install minetest vlc 7zip gimp libreoffice-fresh keepass inkscape firefox thunderbird foxitreader --yes
