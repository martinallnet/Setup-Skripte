# Liesmich für die setup.ps1

Windows Powershell Script um automatisiert mittels Chocolately Programme zu installieren und Windowseinstellungen zu machen.

## Script ausführen:

* Powershell suchen
* rechte Maustaste und "als Administrator ausführen" wählen
* Sicherheitsmeldung bestätigen

### Im Powershell-Fenster

* Befehl "set-executionPolicy -scope process bypass" ausführen.
* Meldung mit "j" und Entertaste bestätigen

Damit wird die Windowssicherheit für dieses Fenster (Session) ausgeschaltet und Du kannst das Script ausführen.

* Script ausführen z.B. "c:\setup.ps1"
* Warten bis die Programme installiert sind. Am Ende kommt eine Erfolgsmeldung, was alles installiert wurde.

## Script Anpassen

### Windows Einstellungen
* to be done


### Chocolatey
Chocolatey ist ein Paketmanager, mit dem Du unter Windows vollautomatisiert Programme installieren kannst.
https://chocolatey.org/
Im Skript wird das Programm installiert und die Installation einiger Programme angestoßen.

Die Installation im Skript erfolgt über "choco install Programmname".
Verfügbare Programmpakete findest Du unter https://community.chocolatey.org/packages


### Windows Apps deinstallieren

Alle Apps deinstalieren (Vorsicht!):
    Get-AppxPackage | Remove-AppxPackage

Alle Apps bis auf einige Ausnahmen deinstallieren (Vorsicht!)

    Get-AppxPackage | where-object {$_.name –notlike “*store*”} | where-object {$_.name –notlike “*communicationsapps*”} | where-object {$_.name –notlike “*people*”} | Remove-AppxPackage

Alle Standardapps wieder installieren
    Get-AppxPackage | foreach {Add-AppxPackage -register „$($_.InstallLocation)\appxmanifest.xml“ -DisableDevelopmentMod} 

Einzelne Apps deinstallieren:

3D Builder 
    Get-AppxPackage *3dbuilder* | Remove-AppxPackage

Alarm und Uhr 
    Get-AppxPackage *windowsalarms* | Remove-AppxPackage

Asphalt 8: Airborne 
    Get-AppxPackage *Asphalt8Airborne* | Remove-AppxPackage

Begleiter für Telefon 	
    Get-AppxPackage *windowsphone* | Remove-AppxPackage

Candy Crush Saga
	Get-AppxPackage *CandyCrushSaga* | Remove-AppxPackage

Drawboard PDF 	
    Get-AppxPackage *DrawboardPDF* | Remove-AppxPackage

Erste Schritte
	Get-AppxPackage *getstarted* | Remove-AppxPackage

Facebook 	
    Get-AppxPackage *Facebook* | Remove-AppxPackage

Feedback Hub
	Get-AppxPackage *feedback* | Remove-AppxPackage

Filme & TV 	
    Get-AppxPackage *zunevideo* | Remove-AppxPackage

Finanzen 	
    Get-AppxPackage *bingfinance* | Remove-AppxPackage

Fotos
	Get-AppxPackage *photos* | Remove-AppxPackage

Groove-Musik
	Get-AppxPackage *zunemusic* | Remove-AppxPackage

Kalender & Mail
	Get-AppxPackage *communicationsapps* | remove-appxpackage

Kamera
	Get-AppxPackage *windowscamera* | Remove-AppxPackage

Karten
	Get-AppxPackage *windowsmaps* | Remove-AppxPackage

Kontakte
	Get-AppxPackage *people* | Remove-AppxPackage

Microsoft Solitaire Collection
	Get-AppxPackage *solitairecollection* | Remove-AppxPackage

Nachrichten
	Get-AppxPackage *bingnews* | Remove-AppxPackage

Nachrichten & Skype 	
    Get-AppxPackage *messaging* | remove-appxpackage

Office holen 	
    Get-AppxPackage *officehub* | Remove-AppxPackage

OneNote 	
    Get-AppxPackage *onenote* | Remove-AppxPackage

Paint 3D 	
    Get-AppxPackage *mspaint* | Remove-AppxPackage

Rechner 	
    Get-AppxPackage *windowscalculator* | Remove-AppxPackage

Skype
	Get-AppxPackage *skypeapp* | Remove-AppxPackage

Sport 	
    Get-AppxPackage *bingsports* | Remove-AppxPackage

Sprachrekorder 	
    Get-AppxPackage *soundrecorder* | Remove-AppxPackage

Windows DVD Player
	Get-AppxPackage *dvd* | Remove-AppxPackage

Xbox Identity Provider
	Get-AppxPackage *xboxIdentityprovider* | Remove-AppxPackage

Xbox 	
    Get-AppxPackage *xboxapp* | Remove-AppxPackage


Weitere Informationen
https://www.deskmodder.de/wiki/index.php/Windows_10_Apps_entfernen_deinstallieren
